package com.thomas.linstol.fragment;

import java.util.ArrayList;
import java.util.List;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.thomas.linstol.MainActivity;
import com.thomas.linstol.R;
import com.thomas.linstol.adapter.CategoryGridAdapter;
import com.thomas.linstol.api.HttpConnectionUtils;
import com.thomas.linstol.datamodel.CategoryItem;
import com.thomas.linstol.datamodel.FeatureItem;
import com.thomas.linstol.datamodel.NotificationItem;
import com.thomas.linstol.datamodel.NotificationModel;

import cn.trinea.android.view.autoscrollviewpager.AutoScrollViewPager;

@SuppressLint("ValidFragment")
public class CategoryListFragment extends Fragment implements
		OnItemClickListener, OnClickListener {
	private View contentView = null;
	private GridView categoryGridView = null;
	private List<CategoryItem> categories = null;
	private List<FeatureItem> sliderImages = null;
    private AutoScrollViewPager autoScrollViewPager;
	private CategoryGridAdapter adapter = null;
	private ArrayList<String> imgUrl=new ArrayList<>();

	private class FetchFeaturedProducts extends
			AsyncTask<String, Void, List<CategoryItem>> {
		protected void onPreExecute() {
			((MainActivity) getActivity())
					.showLoadingDlg("Loading Categories...");
		}

		@Override
		protected List<CategoryItem> doInBackground(String... args) {
			return HttpConnectionUtils.fetchAllCategories();
		}

		@Override
		protected void onPostExecute(List<CategoryItem> result) {
			try {
				((MainActivity) getActivity()).hideLoadingDlg();
			}
			catch(Exception e)
			{

			}
			if (result == null) {
				Toast.makeText(getActivity(), "Connection Error.",
						Toast.LENGTH_LONG).show();
			} else {
				categories = result;
				Log.e("result", "" + categories);
				adapter = new CategoryGridAdapter(getActivity(), categories);
				categoryGridView.setAdapter(adapter);
			}
			new FetchSliderProducts().execute();
		}
	}
	private class FetchNotificationsTask extends
			AsyncTask<String, Void, List<NotificationItem>> {
		@Override
		protected List<NotificationItem> doInBackground(String... args) {
			return HttpConnectionUtils.fetchAllNotifications();
		}

		@Override
		protected void onPostExecute(List<NotificationItem> result) {
			try {
				((MainActivity) getActivity()).hideLoadingDlg();

			if (result == null) {
				((Button) contentView.findViewById(R.id.notificationButton))
						.setText("Notifications (0)");
			} else {
				int newNotifications = 0;
				for (int i = 0; i < result.size(); i++) {
					if (!NotificationModel.isReadNotification(result.get(i)
							.getNotificationId(), getActivity())) {
						newNotifications++;
					}
				}
				((Button) contentView.findViewById(R.id.notificationButton))
						.setText("Notifications (" + newNotifications + ")");
			}
			}
			catch(Exception e)
			{
			}
		}
	}
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		((MainActivity) getActivity()).setTitle("Products");
		contentView = inflater.inflate(R.layout.products_fragment, container,
				false);
		categoryGridView = (GridView) contentView
				.findViewById(R.id.productsGridView);
		autoScrollViewPager=(AutoScrollViewPager)contentView.findViewById(R.id.viewPager);
		categoryGridView.setOnItemClickListener(this);
		((Button) contentView.findViewById(R.id.notificationButton))
				.setOnClickListener(this);

		if (adapter != null) {
			categoryGridView.setAdapter(adapter);
		} else {
			new FetchFeaturedProducts().execute();
		}

		new FetchNotificationsTask().execute();

		return contentView;
	}

	@Override
	public void onItemClick(AdapterView<?> adapter, View itemView,
			int position, long arg3) {
		CategoryItem item = categories.get(position);
		ProductListFragment productListFragment = new ProductListFragment(item, categories);
		((MainActivity) getActivity()).pushFragment(productListFragment);
	}

	@Override
	public void onClick(View v) {
		if (v.getId() == R.id.notificationButton) {
			((MainActivity) getActivity()).gogoInbox();
		}
	}

	private class FetchSliderProducts extends
			AsyncTask<String, Void, List<FeatureItem>> {
		protected void onPreExecute() {
			/*((MainActivity) getActivity())
					.showLoadingDlg("Loading Categories...");*/
		}
		@Override
		protected List<FeatureItem> doInBackground(String... args) {
			return HttpConnectionUtils.fetchFeaturedCategories();
		}

		protected void onPostExecute(List<FeatureItem> result) {
			if (result == null) {
				Toast.makeText(getActivity(), "Connection Error.",
						Toast.LENGTH_LONG).show();
			} else {
				Log.e("result adapter", "" + result);
				sliderImages=result;
				for(int i=0;i<sliderImages.size();i++)
				{
					Log.e("images",sliderImages.get(i).getImgUrl());
				}
				ImageAdapter imageAdapter = new ImageAdapter(getActivity());
				autoScrollViewPager.setAdapter(imageAdapter);
				autoScrollViewPager.startAutoScroll();
				//autoScrollViewPager.setStopScrollWhenTouch(true);
				autoScrollViewPager.setScrollDurationFactor(25);
			}
		}
	}
	public class ImageAdapter extends PagerAdapter {
		Context context;
		ImageAdapter(Context context){
			this.context=context;
		}
		@Override
		public int getCount() {
			return sliderImages.size();
		}

		@Override
		public boolean isViewFromObject(View view, Object object) {
			return view == ((ImageView) object);
		}
		@Override
		public Object instantiateItem(ViewGroup container, int position) {
			ImageView imageView = new ImageView(context);
			//int padding = context.getResources().getDimensionPixelSize(R.dimen.padding_medium);
			//imageView.setPadding(padding, padding, padding, padding);
			imageView.setScaleType(ImageView.ScaleType.FIT_XY);
			//imageView.setImageResource(GalImages[position]);
			Glide.with(context).load(sliderImages.get(position).getImgUrl()).placeholder(R.drawable.loading)
					.thumbnail(0.5f)
					.crossFade()
					.diskCacheStrategy(DiskCacheStrategy.ALL)
					.into(imageView);
			((ViewPager) container).addView(imageView, 0);
			return imageView;
		}

		@Override
		public void destroyItem(ViewGroup container, int position, Object object) {
			((ViewPager) container).removeView((ImageView) object);
		}
	}
}
