package com.thomas.linstol.fragment;

import java.util.List;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.GridView;
import android.widget.Toast;

import com.thomas.linstol.MainActivity;
import com.thomas.linstol.R;
import com.thomas.linstol.adapter.TeamGridAdapter;
import com.thomas.linstol.api.HttpConnectionUtils;
import com.thomas.linstol.datamodel.TeamItem;

@SuppressLint("ValidFragment")
public class TeamListFragment extends Fragment implements OnClickListener {
	private View contentView = null;
	private int regionId = 11;
	private List<TeamItem> teams = null;
	private TeamGridAdapter adapter = null;
	private Button[] regionButtons = null;

	private class FetchTeamsTask extends
			AsyncTask<String, Void, List<TeamItem>> {
		protected void onPreExecute() {
			((MainActivity) getActivity()).showLoadingDlg("Loading Teams...");
		}

		@Override
		protected List<TeamItem> doInBackground(String... args) {
			return HttpConnectionUtils.fetchTeamByRegion(regionId);
		}

		@Override
		protected void onPostExecute(List<TeamItem> result) {
			((MainActivity) getActivity()).hideLoadingDlg();
			if (result == null) {
				Toast.makeText(getActivity(), "Connection Error.",
						Toast.LENGTH_LONG).show();
			} else {
				teams = result;
				adapter = new TeamGridAdapter(getActivity(), teams);
				((GridView) contentView.findViewById(R.id.teamGridView))
						.setAdapter(adapter);
			}
		}
	}

	public TeamListFragment() {
	}
	
	public TeamListFragment(List<TeamItem> team) {
		this.teams = team;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		((MainActivity) getActivity()).setTitle("Management Team");
		contentView = inflater
				.inflate(R.layout.team_fragment, container, false);

		regionButtons = new Button[4];
		regionButtons[0] = (Button) contentView
				.findViewById(R.id.regionGlobalButton);
		regionButtons[1] = (Button) contentView
				.findViewById(R.id.regionUSButton);
		regionButtons[2] = (Button) contentView
				.findViewById(R.id.regionUKButton);
		regionButtons[3] = (Button) contentView
				.findViewById(R.id.regionHKButton);

		for (int i = 0; i < regionButtons.length; i++) {
			regionButtons[i].setOnClickListener(this);
			regionButtons[i].setTag(i + 11);
		}

		if (adapter == null) {
			if (teams != null) {
				contentView.findViewById(R.id.regionLayout).setVisibility(
						View.GONE);
				adapter = new TeamGridAdapter(getActivity(), teams);
				((GridView) contentView.findViewById(R.id.teamGridView))
						.setAdapter(adapter);
			} else {
				new FetchTeamsTask().execute();
			}
		} else {
			((GridView) contentView.findViewById(R.id.teamGridView))
					.setAdapter(adapter);
		}

		return contentView;
	}

	@Override
	public void onClick(View v) {
		int rid = (Integer) v.getTag();
		if (regionId == rid)
			return;

		regionId = rid;
		for (int i = 0; i < regionButtons.length; i++) {
			if (i + 11 == regionId) {
				regionButtons[i].setTextColor(Color.WHITE);
			} else {
				regionButtons[i].setTextColor(Color.rgb(136, 136, 136));
			}
		}

		new FetchTeamsTask().execute();
	}

}
