package com.thomas.linstol.fragment;

import java.util.List;
import java.util.Vector;

import android.annotation.SuppressLint;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.thomas.linstol.MainActivity;
import com.thomas.linstol.R;
import com.thomas.linstol.adapter.InboxListAdapter;
import com.thomas.linstol.api.HttpConnectionUtils;
import com.thomas.linstol.datamodel.NotificationItem;
import com.thomas.linstol.datamodel.NotificationModel;
import com.thomas.linstol.widget.HeaderListView;

@SuppressLint("ValidFragment")
public class InboxListFragment extends Fragment {
	private View contentView = null;
	private List<NotificationItem> notifications = null;
	private InboxListAdapter adapter = null;

	private class FetchNotificationsTask extends
			AsyncTask<String, Void, List<NotificationItem>> {
		protected void onPreExecute() {
			((MainActivity) getActivity())
					.showLoadingDlg("Loading Notifications...");
		}

		@Override
		protected List<NotificationItem> doInBackground(String... args) {
			return HttpConnectionUtils.fetchAllNotifications();
		}

		@Override
		protected void onPostExecute(List<NotificationItem> result) {
			((MainActivity) getActivity()).hideLoadingDlg();
			if (result == null) {
				Toast.makeText(getActivity(), "Connection Error.",
						Toast.LENGTH_LONG).show();
			} else {
				notifications = result;
				rearrangeNotifications();
			}
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		((MainActivity) getActivity()).setTitle("Notifications");
		contentView = inflater.inflate(R.layout.inbox_fragment, container,
				false);

		if (adapter == null) {
			new FetchNotificationsTask().execute();
		} else {
			rearrangeNotifications();
		}

		return contentView;
	}

	public void rearrangeNotifications() {
		Vector<List<NotificationItem>> nn = new Vector<List<NotificationItem>>();

		Vector<NotificationItem> readNotifications = new Vector<NotificationItem>();
		Vector<NotificationItem> unreadNotifications = new Vector<NotificationItem>();

		for (int i = 0; i < notifications.size(); i++) {
			if (NotificationModel.isReadNotification(notifications.get(i)
					.getNotificationId(), getActivity())) {
				readNotifications.add(notifications.get(i));
			} else {
				unreadNotifications.add(notifications.get(i));
			}
		}

		nn.addElement(unreadNotifications);
		nn.addElement(readNotifications);

		adapter = new InboxListAdapter(getActivity(), nn);
		((HeaderListView) contentView.findViewById(R.id.inboxHeaderListView))
				.setAdapter(adapter);
	}

}
