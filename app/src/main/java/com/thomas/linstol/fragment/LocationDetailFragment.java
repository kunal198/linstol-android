package com.thomas.linstol.fragment;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.thomas.linstol.MainActivity;
import com.thomas.linstol.R;
import com.thomas.linstol.datamodel.OfficeItem;

@SuppressLint("ValidFragment")
public class LocationDetailFragment extends Fragment implements OnClickListener{
	private View contentView = null;
	private OfficeItem location;
	private GoogleMap mMap;
	public LocationDetailFragment(OfficeItem location) {
		this.location = location;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
			contentView = inflater.inflate(R.layout.location_detail_fragment, container,
					false);
		mMap = ((SupportMapFragment) getChildFragmentManager()
				.findFragmentById(R.id.mapView)).getMap();
		mMap.addMarker(new MarkerOptions().position(new LatLng(location.getLattitude(), location.getLongtitude())).title(location.getAddress()));
		mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(location.getLattitude(), location.getLongtitude()), 16));
		contentView.findViewById(R.id.backButton).setOnClickListener(this);
		((MainActivity) getActivity()).setTitle("Location");
		this.setLocationInfo(location);
		return contentView;
	}
	private void setLocationInfo(OfficeItem location) {
		((TextView)contentView.findViewById(R.id.countryNameTextView)).setText(location.getLocationName());
		((TextView)contentView.findViewById(R.id.addressTextView)).setText(location.getAddress());
		((TextView)contentView.findViewById(R.id.phoneTextView)).setText(location.getMobile());
		((TextView)contentView.findViewById(R.id.mailTextView)).setText(location.getEmail());
		((TextView)contentView.findViewById(R.id.timezonTextView)).setText(location.getCurrentTime());
		Log.e("details", location.getLongtitude() + "");
	}
	@Override
	public void onClick(View v) {
		if (v.getId() == R.id.backButton) {
			((MainActivity)getActivity()).popFragment();
		}
	}
	@Override
	public void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		/*SupportMapFragment f = (SupportMapFragment)getFragmentManager()
				.findFragmentById(R.id.mapView);
		if (f != null)
			getFragmentManager().beginTransaction().remove(f).commit();*/

	}

}
