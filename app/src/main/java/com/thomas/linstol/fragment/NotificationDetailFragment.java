package com.thomas.linstol.fragment;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.thomas.linstol.MainActivity;
import com.thomas.linstol.R;
import com.thomas.linstol.datamodel.NotificationItem;
import com.thomas.linstol.datamodel.NotificationModel;

@SuppressLint("ValidFragment")
public class NotificationDetailFragment extends Fragment implements
		OnClickListener {
	private View contentView = null;
	private NotificationItem notificationInfo = null;

	public NotificationDetailFragment(NotificationItem notificationInfo) {
		this.notificationInfo = notificationInfo;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		((MainActivity) getActivity()).setTitle("News");

		contentView = inflater.inflate(R.layout.notification_detail_fragment, container,
				false);
		NotificationModel.checkRead(notificationInfo.getNotificationId(), getActivity());

		// configure UI Elements...
		DisplayImageOptions options = new DisplayImageOptions.Builder()
				.cacheOnDisk(true).showImageOnFail(R.drawable.loading)
				.showImageForEmptyUri(R.drawable.loading)
				.showImageOnLoading(R.drawable.loading).build();

		ImageView imgView = (ImageView) contentView
				.findViewById(R.id.notificationImageView);
		ImageLoader.getInstance().displayImage(notificationInfo.getImageUrl(),
				imgView, options);

		((TextView) contentView.findViewById(R.id.dateTextView))
				.setText(notificationInfo.getDateStr());
		((TextView) contentView.findViewById(R.id.titleTextView))
				.setText(notificationInfo.getTitle());
		((TextView) contentView.findViewById(R.id.detailTextView))
				.setText(notificationInfo.getContent());

		contentView.findViewById(R.id.backButton).setOnClickListener(this);
		contentView.findViewById(R.id.contactUsButton).setOnClickListener(this);

		return contentView;
	}

	@Override
	public void onClick(View v) {
		if (v.getId() == R.id.backButton) {
			((MainActivity) getActivity()).popFragment();
		} else if (v.getId() == R.id.contactUsButton) {
			((MainActivity) getActivity()).gotoContactUs();
		}
	}

}
