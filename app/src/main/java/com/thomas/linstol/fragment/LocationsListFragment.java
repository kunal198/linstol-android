package com.thomas.linstol.fragment;

import java.util.List;

import android.annotation.SuppressLint;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;
import android.widget.Toast;

import com.thomas.linstol.MainActivity;
import com.thomas.linstol.R;
import com.thomas.linstol.adapter.LocationsGridAdapter;
import com.thomas.linstol.api.HttpConnectionUtils;
import com.thomas.linstol.datamodel.OfficeItem;

@SuppressLint("ValidFragment")
public class LocationsListFragment extends Fragment implements
		OnItemClickListener {
	private View contentView = null;
	private List<OfficeItem> locations = null;
	private LocationsGridAdapter adapter = null;

	private class FetchLocationsTask extends
			AsyncTask<String, Void, List<OfficeItem>> {
		protected void onPreExecute() {
			((MainActivity) getActivity()).showLoadingDlg("Loading Locations...");
		}

		@Override
		protected List<OfficeItem> doInBackground(String... args) {
			return HttpConnectionUtils.fetchAllOffices();
		}

		@Override
		protected void onPostExecute(List<OfficeItem> result) {
			((MainActivity) getActivity()).hideLoadingDlg();
			if (result == null) {
				Toast.makeText(getActivity(), "Connection Error.",
						Toast.LENGTH_LONG).show();
			} else {
				locations = result;
				Log.e("result", ""+locations);
				adapter = new LocationsGridAdapter(getActivity(), locations);
				((GridView) contentView.findViewById(R.id.locationGridView))
						.setAdapter(adapter);
				((GridView) contentView.findViewById(R.id.locationGridView))
						.setOnItemClickListener(LocationsListFragment.this);
			}
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		((MainActivity) getActivity()).setTitle("Locations");
		contentView = inflater.inflate(R.layout.locations_fragment, container,
				false);

		if (adapter == null) {
			new FetchLocationsTask().execute();
		} else {
			((GridView) contentView.findViewById(R.id.locationGridView))
					.setAdapter(adapter);
		}

		((GridView) contentView.findViewById(R.id.locationGridView))
				.setOnItemClickListener(this);

		return contentView;
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int position,
			long arg3) {
		OfficeItem item = locations.get(position);
		LocationDetailFragment detailFragment = new LocationDetailFragment(item);
		((MainActivity) getActivity()).pushFragment(detailFragment);
	}
}
