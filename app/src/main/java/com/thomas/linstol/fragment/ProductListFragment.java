package com.thomas.linstol.fragment;

import java.util.List;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.SearchView;
import android.widget.SearchView.OnQueryTextListener;
import android.widget.Toast;

import com.thomas.linstol.MainActivity;
import com.thomas.linstol.R;
import com.thomas.linstol.adapter.ProductGridAdapter;
import com.thomas.linstol.api.HttpConnectionUtils;
import com.thomas.linstol.datamodel.CategoryItem;
import com.thomas.linstol.datamodel.ProductItem;

@SuppressLint("ValidFragment")
public class ProductListFragment extends Fragment implements
		OnItemClickListener, OnQueryTextListener, OnClickListener {
	private View contentView = null;
	private SearchView searchView = null;
	private CategoryItem categoryInfo = null;
	private List<ProductItem> products = null;
	private List<CategoryItem> categories = null;
	private Button[] menuButtons = null;
	private ProductGridAdapter adapter = null;

	private class FetchFeaturedProducts extends
			AsyncTask<String, Void, List<ProductItem>> {
		protected void onPreExecute() {
			((MainActivity) getActivity())
					.showLoadingDlg("Loading Products...");
		}
		@Override
		protected List<ProductItem> doInBackground(String... args) {
			return HttpConnectionUtils.fetchProductsByCategory(args[0]);
		}
		@Override
		protected void onPostExecute(List<ProductItem> result) {
			((MainActivity) getActivity()).hideLoadingDlg();
			if (result == null) {
				Toast.makeText(getActivity(), "Connection Error.",
						Toast.LENGTH_LONG).show();
			} else {
				products = result;
				adapter = new ProductGridAdapter(getActivity(), products);
				((GridView) contentView.findViewById(R.id.productsGridView))
						.setAdapter(adapter);
				((GridView) contentView.findViewById(R.id.productsGridView))
						.setOnItemClickListener(ProductListFragment.this);
			}
		}
	}

	public ProductListFragment(CategoryItem categoryInfo, List<CategoryItem> categories) {
		this.categoryInfo = categoryInfo;
		this.categories = categories;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		((MainActivity) getActivity()).setTitle(categoryInfo.getCategoryName());
		contentView = inflater.inflate(R.layout.products_list_fragment, container,
				false);

		if (adapter == null) {
			new FetchFeaturedProducts().execute(categoryInfo.getCategoryId());
		} else {
			((GridView) contentView.findViewById(R.id.productsGridView))
					.setAdapter(adapter);
			((GridView) contentView.findViewById(R.id.productsGridView))
					.setOnItemClickListener(this);
		}
		
		this.initCategoryMenu();
		this.selectMenu(this.categoryInfo);

		return contentView;
	}
	
	private void initCategoryMenu() {
		menuButtons = new Button[this.categories.size()];
		for (int i=0; i<menuButtons.length; i++) {
			Button menuButton = new Button(getActivity());
			Log.e("textt",categories.get(i).getCategoryName());
			menuButton.setText(categories.get(i).getCategoryName());
			menuButton.setBackgroundColor(Color.rgb(197, 32, 51));
			menuButton.setTextColor(Color.WHITE);
			menuButton.setOnClickListener(this);
			menuButton.setTag(i);
			
			LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
			menuButton.setLayoutParams(param);
			menuButtons[i] = menuButton;
			
			((LinearLayout)contentView.findViewById(R.id.categoriesLayout)).addView(menuButton);
		}
	}
	
	private void selectMenu(CategoryItem category) {
		for (int i=0; i<categories.size(); i++) {
			if (categories.get(i).getCategoryId().equals(category.getCategoryId())) {
				menuButtons[i].setVisibility(View.GONE);
			} else {
				menuButtons[i].setVisibility(View.VISIBLE);
			}
		}
	}
	@Override
	public void onItemClick(AdapterView<?> adapter, View itemView,
			int position, long arg3) {
		ProductItem item = products.get(position);
		Log.e("click","clikck"+products.get(position));
		ProductDetailFragment detailFragment = new ProductDetailFragment(item);
		((MainActivity) getActivity()).pushFragment(detailFragment);
	}

	@Override
	public boolean onQueryTextChange(String arg0) {
		return false;
	}

	@Override
	public boolean onQueryTextSubmit(String query) {
		searchView.clearFocus();
		if (!query.equals("")) {
			new FetchFeaturedProducts().execute(query);
		}

		return false;
	}

	public void onStop() {
		if (searchView != null)
			searchView.clearFocus();
		super.onStop();
	}

	@Override
	public void onClick(View v) {
		CategoryItem category = this.categories.get((int) v.getTag());
		this.selectMenu(category);
		new FetchFeaturedProducts().execute(category.getCategoryId());
	}

}
