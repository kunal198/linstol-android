package com.thomas.linstol.fragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.thomas.linstol.MainActivity;
import com.thomas.linstol.R;
import com.thomas.linstol.datamodel.ProductItem;

import cn.trinea.android.view.autoscrollviewpager.AutoScrollViewPager;

@SuppressLint("ValidFragment")
public class ProductDetailFragment extends Fragment implements OnClickListener {
	private View contentView = null;
	private ProductItem productInfo = null;
	private AutoScrollViewPager autoScrollViewPager;

	public ProductDetailFragment(ProductItem productInfo) {
		this.productInfo = productInfo;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		((MainActivity) getActivity()).setTitle(productInfo.getProductName());

		contentView = inflater.inflate(R.layout.pitem_fragment, container,
				false);

		ImageAdapter adapter = new ImageAdapter(getActivity());
		autoScrollViewPager = (AutoScrollViewPager) contentView.findViewById(R.id.viewPager);
		autoScrollViewPager.setAdapter(adapter);

		// configure UI Elements...
		DisplayImageOptions options = new DisplayImageOptions.Builder()
				.cacheOnDisk(true).showImageOnFail(R.drawable.loading)
				.showImageForEmptyUri(R.drawable.loading)
				.showImageOnLoading(R.drawable.loading).build();

		ImageView imgView = (ImageView) contentView
				.findViewById(R.id.photoImageView);
		ImageLoader.getInstance().displayImage(productInfo.getOrgPhotoUrl(),
				imgView, options);

		((TextView) contentView.findViewById(R.id.productNameLabel))
				.setText(productInfo.getProductName());
		((WebView) contentView.findViewById(R.id.productDetailView)).loadData(
				productInfo.getProductDetails(), "text/html", "UTF-8");

		contentView.findViewById(R.id.backButton).setOnClickListener(this);
		contentView.findViewById(R.id.contactUsTextView).setOnClickListener(this);

		return contentView;
	}

	@Override
	public void onClick(View v) {
		if (v.getId() == R.id.backButton) {
			((MainActivity)getActivity()).popFragment();
		} else if (v.getId() == R.id.contactUsTextView) {
			((MainActivity) getActivity()).gotoContactUs();
		}
	}
	public class ImageAdapter extends PagerAdapter {
		Context context;
		private int[] GalImages = new int[] {
				R.drawable.ic_launcher,
				R.drawable.ic_launcher,
				R.drawable.ic_launcher
		};
		ImageAdapter(Context context){
			this.context=context;
		}
		@Override
		public int getCount() {
			return GalImages.length;
		}

		@Override
		public boolean isViewFromObject(View view, Object object) {
			return view == ((ImageView) object);
		}
		@Override
		public Object instantiateItem(ViewGroup container, int position) {
			ImageView imageView = new ImageView(context);
			//int padding = context.getResources().getDimensionPixelSize(R.dimen.padding_medium);
			//imageView.setPadding(padding, padding, padding, padding);
			imageView.setScaleType(ImageView.ScaleType.FIT_XY);
			imageView.setImageResource(GalImages[position]);

			((ViewPager) container).addView(imageView, 0);
			return imageView;
		}

		@Override
		public void destroyItem(ViewGroup container, int position, Object object) {
			((ViewPager) container).removeView((ImageView) object);
		}
	}
}
