package com.thomas.linstol.fragment;

import java.util.List;
import java.util.Vector;

import android.annotation.SuppressLint;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SearchView;
import android.widget.SearchView.OnQueryTextListener;
import android.widget.Toast;

import com.thomas.linstol.MainActivity;
import com.thomas.linstol.R;
import com.thomas.linstol.adapter.SearchListAdapter;
import com.thomas.linstol.api.HttpConnectionUtils;
import com.thomas.linstol.datamodel.CategoryItem;
import com.thomas.linstol.widget.HeaderListView;

@SuppressLint("ValidFragment")
public class SearchFragment extends Fragment implements OnQueryTextListener {
	private View contentView = null;
	private SearchView searchView = null;
	private List<List<?>> searchResult = null;
	private SearchListAdapter adapter = null;

	private class SearchTask extends AsyncTask<String, Void, List<List<?>>> {
		protected void onPreExecute() {
			((MainActivity) getActivity())
					.showLoadingDlg("Searching ...");
		}
		@Override
		protected List<List<?>> doInBackground(String... args) {
			return HttpConnectionUtils.fetchProductsByKeyword(args[0]);
		}
		@Override
		protected void onPostExecute(List<List<?>> result) {
			((MainActivity) getActivity()).hideLoadingDlg();
			/*if (result == null) {
				Toast.makeText(getActivity(), "Connection Error.",
						Toast.LENGTH_LONG).show();
			} else {
				Log.e("resultt", ""+searchResult);
				if (searchResult == null) {
					searchResult = new Vector<List<?>>();
					searchResult.add(new Vector<Object>());
					searchResult.add(new Vector<Object>());
					searchResult.add(new Vector<Object>());
					searchResult.add(new Vector<Object>());
				}

			}*/
			searchResult = result;
			adapter = new SearchListAdapter(getActivity(), searchResult);
			((HeaderListView) contentView.findViewById(R.id.searchListView))
					.setAdapter(adapter);
		}
	}
	public SearchFragment(CategoryItem categoryInfo) {
	}

	public SearchFragment() {
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		((MainActivity) getActivity()).setTitle("Search");
		contentView = inflater.inflate(R.layout.search_fragment, container,
				false);

		if (adapter != null) {
			((HeaderListView) contentView.findViewById(R.id.searchListView))
					.setAdapter(adapter);
		}

		searchView = (SearchView) contentView
				.findViewById(R.id.keywordSearchView);
		searchView.setOnQueryTextListener(this);

		return contentView;
	}

	@Override
	public boolean onQueryTextChange(String arg0) {
		return false;
	}

	@Override
	public boolean onQueryTextSubmit(String query) {
		searchView.clearFocus();
		if (!query.equals("")) {
			new SearchTask().execute(query);
		}

		return false;
	}

	public void onStop() {
		if (searchView != null)
			searchView.clearFocus();
		super.onStop();
	}

}
