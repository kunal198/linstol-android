package com.thomas.linstol;

import java.util.Vector;

import android.app.ProgressDialog;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.thomas.linstol.fragment.CategoryListFragment;
import com.thomas.linstol.fragment.InboxListFragment;
import com.thomas.linstol.fragment.LocationsListFragment;
import com.thomas.linstol.fragment.SearchFragment;
import com.thomas.linstol.fragment.TeamListFragment;

public class MainActivity extends FragmentActivity implements OnClickListener {
	private Button[] tabButtons = null;
	private ProgressDialog loading_dlg = null;
	private Vector<Vector<Fragment>> fragmentArray = null;
	private int curTabIndex = 0;

	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.main_activity);
		
		findViewById(R.id.logoImageView).setOnClickListener(this);

		initUI();
		loadFragments();
	}

	private void loadFragments() {
		fragmentArray = new Vector<Vector<Fragment>>();

		// Products Tab
		Vector<Fragment> productsTab = new Vector<Fragment>();
		CategoryListFragment categoryFrag = new CategoryListFragment();
		productsTab.add(categoryFrag);

		fragmentArray.add(productsTab);

		FragmentManager fragmentManager = getSupportFragmentManager();
		fragmentManager.beginTransaction()
				.replace(R.id.containerLayout, categoryFrag).commit();

		// SearchTab
		Vector<Fragment> searchTab = new Vector<Fragment>();
		SearchFragment searchFragment = new SearchFragment();
		searchTab.add(searchFragment);
		fragmentArray.add(searchTab);

		// Inbox
		Vector<Fragment> inboxTab = new Vector<Fragment>();
		InboxListFragment inboxFragment = new InboxListFragment();
		inboxTab.add(inboxFragment);
		fragmentArray.add(inboxTab);
		
		// SearchTab
		Vector<Fragment> teamTab = new Vector<Fragment>();
		TeamListFragment teamFragment = new TeamListFragment();
		teamTab.add(teamFragment);
		fragmentArray.add(teamTab);

		// Locations Tab
		Vector<Fragment> locationTab = new Vector<Fragment>();
		LocationsListFragment locationFragment = new LocationsListFragment();
		locationTab.add(locationFragment);
		fragmentArray.add(locationTab);
	}

	private void initUI() {
		tabButtons = new Button[5];
		tabButtons[0] = (Button) findViewById(R.id.btnTabProducts);
		tabButtons[1] = (Button) findViewById(R.id.btnTabSearch);
		tabButtons[2] = (Button) findViewById(R.id.btnInbox);
		tabButtons[3] = (Button) findViewById(R.id.btnTabTeam);
		tabButtons[4] = (Button) findViewById(R.id.btnTabContacts);

		for (int i = 0; i < tabButtons.length; i++) {
			tabButtons[i].setOnClickListener(this);
			tabButtons[i].setTag(i);
		}
	}
	
	public void gotoContactUs() {
		this.selectTabMenu(tabButtons[4]);
	}

	public void gogoInbox() {
		this.selectTabMenu(tabButtons[2]);
	}
	
	private void selectTabMenu(Button curTabButton) {
		for (int i = 0; i < tabButtons.length; i++) {
			tabButtons[i].setBackgroundColor(Color.argb(0, 0, 0, 0));
		}

		curTabButton.setBackgroundColor(Color.rgb(197, 32, 51));
		if (((Integer) curTabButton.getTag()) == curTabIndex) {
			this.popFragment();
		} else {
			curTabIndex = (Integer) curTabButton.getTag();
			if (fragmentArray.size() <= curTabIndex)
				return;

			Fragment fg = fragmentArray.get(curTabIndex).lastElement();
			FragmentTransaction ft = getSupportFragmentManager()
					.beginTransaction();
			ft.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_in_left);
			ft.replace(R.id.containerLayout, fg, "fragment");
			ft.commit();
		}
	}

	@Override
	public void onClick(View v) {
		if (v.getId() == R.id.logoImageView) {
			selectTabMenu(tabButtons[0]);
		} else if (v instanceof Button) {
			selectTabMenu((Button) v);
		}
	}

	public void showLoadingDlg(String title) {
		if (loading_dlg != null)
			return;
		loading_dlg = ProgressDialog.show(this, title, "Please wait...");
		loading_dlg.setCancelable(false);
	}

	public void hideLoadingDlg() {
		if (loading_dlg != null) {
			try {
				loading_dlg.dismiss();
			} catch (Exception e) {
			}
			loading_dlg = null;
		}
	}

	public void pushFragment(Fragment fg) {
		if (fragmentArray.size() <= curTabIndex)
			return;

		FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
		fragmentArray.get(curTabIndex).addElement(fg);

		ft.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_in_left);
		ft.replace(R.id.containerLayout, fg, "fragment");
		ft.commit();
	}

	public void popFragment() {
		int size = fragmentArray.get(curTabIndex).size();
		if (size <= 1)
			return;
		fragmentArray.get(curTabIndex).removeElementAt(size - 1);

		FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
		Fragment fg = fragmentArray.get(curTabIndex).get(size - 2);

		ft.setCustomAnimations(R.anim.slide_out_left, R.anim.slide_out_right);
		ft.replace(R.id.containerLayout, fg, "fragment");
		ft.commit();
	}

	public void setTitle(String title) {
		((TextView) findViewById(R.id.titleLabel)).setText(title);
	}
}
