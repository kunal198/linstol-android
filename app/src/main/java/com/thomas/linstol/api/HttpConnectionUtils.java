package com.thomas.linstol.api;

import android.util.Log;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.security.KeyStore;
import java.util.List;
import java.util.Vector;

import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.protocol.HTTP;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.thomas.linstol.datamodel.CategoryItem;
import com.thomas.linstol.datamodel.FeatureItem;
import com.thomas.linstol.datamodel.NotificationItem;
import com.thomas.linstol.datamodel.OfficeItem;
import com.thomas.linstol.datamodel.ProductItem;
import com.thomas.linstol.datamodel.TeamItem;

public class HttpConnectionUtils {
	private static final String SERVER_URL = "http://www.linstol.com/api/index";
	private static final String API_URL_CATEGORIES = "/categories";
	private static final String API_URL_PRODUCTSBYCATEGORY = "/fetchProductsByCategory";
	private static final String API_URL_PRODUCTSBYKEYWORD = "/fetchProductsByKeyword";
	private static final String API_URL_TEAMSBYREGION = "/fetchTeamByRegion";
	private static final String API_URL_OFFICES = "/fetchAllOffices";
	private static final String API_URL_NOTIFICATIONS = "/notifications";
	private static final String API_URL_FEATURED = "/featured";

	public static List<CategoryItem> fetchAllCategories() {
		String url = SERVER_URL + API_URL_CATEGORIES;
		String jsonStr = connectGetToServer(url);
		Vector<CategoryItem> resArr = new Vector<CategoryItem>();
		try {
			JSONArray jsonArr = new JSONArray(jsonStr);
			for (int i = 0; i < jsonArr.length(); i++) {
				CategoryItem item = new CategoryItem(jsonArr.getJSONObject(i));
				resArr.addElement(item);
			}
		} catch (JSONException e) {
		}
		return resArr;
	}

	public static List<FeatureItem> fetchFeaturedCategories() {
		String url = SERVER_URL + API_URL_FEATURED;
		String jsonStr = connectGetToServer(url);
		Vector<FeatureItem> resArr = new Vector<FeatureItem>();
		try {
			JSONArray jsonArr = new JSONArray(jsonStr);
			Log.e("arrayy",jsonStr);
			for (int i = 0; i < jsonArr.length(); i++) {
				FeatureItem item = new FeatureItem(jsonArr.getJSONObject(i));
				resArr.addElement(item);
			}
		} catch (JSONException e) {
		}
		return resArr;
	}

	public static List<ProductItem> fetchProductsByCategory(String categoryId) {
		String url = SERVER_URL + API_URL_PRODUCTSBYCATEGORY + "?categoryId="
				+ categoryId;
		String jsonStr = connectGetToServer(url);
		Vector<ProductItem> resArr = new Vector<ProductItem>();

		Log.e("details",url);

		try {
			JSONArray jsonArr = new JSONArray(jsonStr);
			for (int i = 0; i < jsonArr.length(); i++) {
				ProductItem item = new ProductItem(jsonArr.getJSONObject(i));
				resArr.addElement(item);
			}
		} catch (JSONException e) {
		}

		return resArr;
	}

	public static List<List<?>> fetchProductsByKeyword(String keyword) {
		String url = null;
		try {
			url = SERVER_URL + API_URL_PRODUCTSBYKEYWORD + "?keyword="
					+ URLEncoder.encode(keyword, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			return null;
		}

		String jsonStr = connectGetToServer(url);
		JSONObject jsonObj = null;
		try {
			jsonObj = new JSONObject(jsonStr);
		} catch (JSONException e1) {
			return null;
		}
		
		Vector<List<?>> resArr = new Vector<List<?>>();
		Log.e("jsonobj", ""+jsonObj);
		
		// Products
		Vector<Object> products = new Vector<Object>();
		JSONArray productsArray=null;
		try {
			 productsArray = jsonObj.getJSONArray("products");
			for (int i = 0; i < productsArray.length(); i++) {
				ProductItem item = new ProductItem(productsArray.getJSONObject(i));
				products.addElement(item);
			}
		} catch (JSONException e) {
		}
		Log.e("products",productsArray.length()+"");
		//if(productsArray.length()>=1)
			resArr.addElement(products);

		resArr.addElement(new Vector<Object>());	// Notifications...

		// Teams
		Vector<Object> teams = new Vector<Object>();
		JSONArray teamsArray=null;
		try {
			 teamsArray = jsonObj.getJSONArray("teams");
			for (int i = 0; i < teamsArray.length(); i++) {
				TeamItem item = new TeamItem(teamsArray.getJSONObject(i));
				teams.addElement(item);
			}
		} catch (JSONException e) {
		}
		Log.e("team",teamsArray.length()+"");

		//if(teamsArray.length()>=1)
			resArr.addElement(teams);
		
		// Locations
		Vector<Object> locations = new Vector<Object>();
		JSONArray locationsArray = null;
		try {
			 locationsArray = jsonObj.getJSONArray("locations");
			for (int i = 0; i < locationsArray.length(); i++) {
				OfficeItem item = new OfficeItem(locationsArray.getJSONObject(i));
				locations.addElement(item);
			}
		} catch (JSONException e) {
		}
		Log.e("locations",locationsArray.length()+"");


		//if(locationsArray.length()>=1)
		resArr.addElement(locations);

		return resArr;
	}

	public static List<TeamItem> fetchTeamByRegion(int regionId) {
		String url = SERVER_URL + API_URL_TEAMSBYREGION + "?region=" + regionId;
		String jsonStr = connectGetToServer(url);
		Vector<TeamItem> resArr = new Vector<TeamItem>();

		try {
			JSONArray jsonArr = new JSONArray(jsonStr);
			for (int i = 0; i < jsonArr.length(); i++) {
				TeamItem item = new TeamItem(jsonArr.getJSONObject(i));
				resArr.addElement(item);
			}
		} catch (JSONException e) {
		}

		return resArr;
	}

	public static List<OfficeItem> fetchAllOffices() {
		String url = SERVER_URL + API_URL_OFFICES;
		String jsonStr = connectGetToServer(url);
		Vector<OfficeItem> resArr = new Vector<OfficeItem>();
		Log.e("result",""+jsonStr);

		try {
			JSONArray jsonArr = new JSONArray(jsonStr);
			for (int i = 0; i < jsonArr.length(); i++) {
				OfficeItem item = new OfficeItem(jsonArr.getJSONObject(i));
				resArr.addElement(item);
			}
		} catch (JSONException e) {
		}

		return resArr;
	}

	public static List<NotificationItem> fetchAllNotifications() {
		String url = SERVER_URL + API_URL_NOTIFICATIONS;
		String jsonStr = connectGetToServer(url);
		Vector<NotificationItem> resArr = new Vector<NotificationItem>();

		try {
			JSONArray jsonArr = new JSONArray(jsonStr);
			for (int i = 0; i < jsonArr.length(); i++) {
				NotificationItem item = new NotificationItem(
						jsonArr.getJSONObject(i));
				resArr.addElement(item);
			}
		} catch (JSONException e) {
		}

		return resArr;
	}

	public static String connectGetToServer(String url) {
		HttpClient httpclient = getNewHttpClient();
		HttpGet httpget = new HttpGet(url);
		httpget.setHeader("Language", "en");

		HttpResponse response = null;
		try {
			response = httpclient.execute(httpget);
			InputStream istream = response.getEntity().getContent();
			return convertStreamToString(istream);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	private static DefaultHttpClient getNewHttpClient() {
		try {
			KeyStore trustStore = KeyStore.getInstance(KeyStore
					.getDefaultType());
			trustStore.load(null, null);

			SSLSocketFactory sf = new EasySSLSocketFactory(trustStore);
			sf.setHostnameVerifier(SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);

			HttpParams params = new BasicHttpParams();
			HttpProtocolParams.setVersion(params, HttpVersion.HTTP_1_1);
			HttpProtocolParams.setContentCharset(params, HTTP.UTF_8);

			SchemeRegistry registry = new SchemeRegistry();
			registry.register(new Scheme("http", PlainSocketFactory
					.getSocketFactory(), 80));
			registry.register(new Scheme("https", sf, 443));

			ClientConnectionManager ccm = new ThreadSafeClientConnManager(
					params, registry);

			return new DefaultHttpClient(ccm, params);
		} catch (Exception e) {
			return new DefaultHttpClient();
		}
	}

	private static String convertStreamToString(InputStream is)
			throws Exception {
		BufferedReader reader = new BufferedReader(new InputStreamReader(is));
		StringBuffer sb = new StringBuffer();
		String line = null;
		while ((line = reader.readLine()) != null) {
			sb.append(line + "\n");
		}

		return sb.toString();
	}
}
