package com.thomas.linstol.adapter;

import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.TextView;

import com.thomas.linstol.MainActivity;
import com.thomas.linstol.R;
import com.thomas.linstol.datamodel.NotificationItem;
import com.thomas.linstol.fragment.NotificationDetailFragment;
import com.thomas.linstol.widget.SectionAdapter;

public class InboxListAdapter extends SectionAdapter {

	private List<List<NotificationItem>> notifications = null;
	private Context mContext = null;

	public InboxListAdapter(Context context,
			List<List<NotificationItem>> notifications) {
		this.mContext = context;
		this.notifications = notifications;
	}

	@Override
	public int numberOfSections() {
		return notifications.size();
	}

	@Override
	public int numberOfRows(int section) {
		if (section >= 0 && section < notifications.size()) {
			return notifications.get(section).size();
		} else {
			return 0;
		}
	}

	@Override
	public boolean hasSectionHeaderView(int section) {
		return section >= 0 && section < notifications.size();
	}

	@Override
	public View getRowView(int section, int row, View convertView,
			ViewGroup parent) {
		convertView = (TextView) ((Activity) mContext).getLayoutInflater()
				.inflate(R.layout.inbox_row_item, null);
		((TextView) convertView).setText(notifications.get(section).get(row)
				.getTitle());
		if (section == 0) {
			((TextView) convertView).setBackgroundColor(Color.rgb(197, 32, 51));
		} else {
			((TextView) convertView).setBackgroundColor(Color
					.rgb(200, 200, 200));
		}
		return convertView;
	}

	@Override
	public Object getRowItem(int section, int row) {
		return null;
	}

	@Override
	public int getSectionHeaderViewTypeCount() {
		return 1;
	}

	@Override
	public int getSectionHeaderItemViewType(int section) {
		return 0;
	}

	@Override
	public View getSectionHeaderView(int section, View convertView,
			ViewGroup parent) {
		if (convertView == null) {
			convertView = (TextView) ((Activity) mContext).getLayoutInflater()
					.inflate(R.layout.inbox_section_item, null);
		}

		if (section == 0) {
			((TextView) convertView).setText("Un-Read");
		} else {
			((TextView) convertView).setText("Read");
		}

		return convertView;
	}

	@Override
	public void onRowItemClick(AdapterView<?> parent, View view, int section,
			int row, long id) {
		NotificationItem item = notifications.get(section).get(row);
		NotificationDetailFragment detailFragment = new NotificationDetailFragment(
				item);
		((MainActivity) mContext).pushFragment(detailFragment);
	}

}
