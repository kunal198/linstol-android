package com.thomas.linstol.adapter;

import java.util.List;
import java.util.Vector;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.thomas.linstol.R;
import com.thomas.linstol.datamodel.CategoryItem;

public class CategoryGridAdapter extends BaseAdapter {
	List<CategoryItem> categories = null;
	Vector<View> viewArray = null;

	private Context mContext;

	// Gets the context so it can be used later
	public CategoryGridAdapter(Context c, List<CategoryItem> categories) {
		mContext = c;
		this.categories = categories;
		viewArray = new Vector<View>();
		for (int i = 0; i < categories.size(); i++) {
			viewArray.addElement(null);
		}
	}

	// Total number of things contained within the adapter
	public int getCount() {
		return categories.size();
	}

	// Require for structure, not really used in my code.
	public Object getItem(int position) {
		return null;
	}

	// Require for structure, not really used in my code. Can
	// be used to get the id of an item in the adapter for
	// manual control.
	public long getItemId(int position) {
		return position;
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		View row = viewArray.elementAt(position);
		if (row == null) {
			LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
			row = inflater.inflate(R.layout.product_item, parent, false);

			viewArray.setElementAt(row, position);
			DisplayImageOptions options = new DisplayImageOptions.Builder()
					.cacheOnDisk(true).build();

			CategoryItem item = categories.get(position);
			ImageView imgView = (ImageView) row
					.findViewById(R.id.photoImageView);
			ImageLoader.getInstance().displayImage(item.getPhotoUrl(), imgView,
					options);
			((TextView) row.findViewById(R.id.titleLabel)).setText(item
					.getCategoryName());
		}

		return row;
	}
}