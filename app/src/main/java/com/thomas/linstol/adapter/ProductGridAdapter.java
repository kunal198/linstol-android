package com.thomas.linstol.adapter;

import java.util.List;
import java.util.Vector;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.thomas.linstol.R;
import com.thomas.linstol.datamodel.ProductItem;

public class ProductGridAdapter extends BaseAdapter {
	List<ProductItem> products = null;
	Vector<View> viewArray = null;

	private Context mContext;

	// Gets the context so it can be used later
	public ProductGridAdapter(Context c, List<ProductItem> products) {
		mContext = c;
		this.products = products;
		viewArray = new Vector<View>();
		for (int i = 0; i < products.size(); i++) {
			viewArray.addElement(null);
		}
	}

	// Total number of things contained within the adapter
	public int getCount() {
		return products.size();
	}

	// Require for structure, not really used in my code.
	public Object getItem(int position) {
		return null;
	}

	// Require for structure, not really used in my code. Can
	// be used to get the id of an item in the adapter for
	// manual control.
	public long getItemId(int position) {
		return position;
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		View row = viewArray.elementAt(position);
		if (row == null) {
			LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
			row = inflater.inflate(R.layout.product_item, parent, false);

			viewArray.setElementAt(row, position);
			DisplayImageOptions options = new DisplayImageOptions.Builder()
					.cacheOnDisk(true).showImageOnFail(R.drawable.loading)
					.showImageForEmptyUri(R.drawable.loading)
					.showImageOnLoading(R.drawable.loading).build();

			ProductItem item = products.get(position);
			ImageView imgView = (ImageView) row
					.findViewById(R.id.photoImageView);
			ImageLoader.getInstance().displayImage(item.getPhotoUrl(), imgView,
					options);
			((TextView) row.findViewById(R.id.titleLabel)).setText(item
					.getProductName());
		}

		return row;
	}
}