package com.thomas.linstol.adapter;

import java.util.List;
import java.util.Vector;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.thomas.linstol.MainActivity;
import com.thomas.linstol.R;
import com.thomas.linstol.datamodel.OfficeItem;
import com.thomas.linstol.fragment.LocationDetailFragment;

public class LocationsGridAdapter extends BaseAdapter implements OnClickListener {
	List<OfficeItem> locations = null;
	Vector<View> viewArray = null;

	private Context mContext;

	// Gets the context so it can be used later
	public LocationsGridAdapter(Context c, List<OfficeItem> locations) {
		mContext = c;
		this.locations = locations;
		viewArray = new Vector<View>();
		for (int i = 0; i < locations.size(); i++) {
			viewArray.addElement(null);
		}
	}

	// Total number of things contained within the adapter
	public int getCount() {
		return locations.size();
	}

	// Require for structure, not really used in my code.
	public Object getItem(int position) {
		return null;
	}

	// Require for structure, not really used in my code. Can
	// be used to get the id of an item in the adapter for
	// manual control.
	public long getItemId(int position) {
		return position;
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		View row = viewArray.elementAt(position);
		if (row == null) {
			LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
			row = inflater.inflate(R.layout.location_item, parent, false);

			viewArray.setElementAt(row, position);
			OfficeItem item = locations.get(position);
			
			((TextView)row.findViewById(R.id.countryNameTextView)).setText(item.getLocationName());
			((TextView)row.findViewById(R.id.addressTextView)).setText(item.getAddress());
			((TextView)row.findViewById(R.id.phoneTextView)).setText(item.getMobile());
			((TextView)row.findViewById(R.id.timezonTextView)).setText(item.getCurrentTime());
			((ImageView)row.findViewById(R.id.locationImageView)).setImageResource(item.getMapImageId());
			
			String mailLink = "<a href=\"mailto:" + item.getEmail() + "\">"
					+ item.getEmail() + "</a>";
			((TextView) row.findViewById(R.id.mailTextView)).setText(Html
					.fromHtml(mailLink));
			((TextView) row.findViewById(R.id.mailTextView))
					.setMovementMethod(LinkMovementMethod.getInstance());

			row.findViewById(R.id.phoneTextView).setOnClickListener(this);
			row.findViewById(R.id.phoneTextView).setTag(position);
			
			row.findViewById(R.id.locationButton).setOnClickListener(this);
			row.findViewById(R.id.locationButton).setTag(position);
			
			row.findViewById(R.id.locationImageView).setOnClickListener(this);
			row.findViewById(R.id.locationImageView).setTag(position);
		}

		return row;
	}

	@Override
	public void onClick(View v) {
		OfficeItem item = locations.get((Integer) v.getTag());
		if (v.getId() == R.id.phoneNumberLabel) {
			Intent intent = new Intent(Intent.ACTION_CALL);
			intent.setData(Uri.parse("tel:" + item.getMobile()));
			mContext.startActivity(intent);
		} else if (v.getId() == R.id.locationButton || v.getId() == R.id.locationImageView) {
			LocationDetailFragment detailFragment = new LocationDetailFragment(item);
			((MainActivity) mContext).pushFragment(detailFragment);
		}
	}

}