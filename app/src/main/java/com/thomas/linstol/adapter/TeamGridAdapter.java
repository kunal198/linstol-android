package com.thomas.linstol.adapter;

import java.util.List;
import java.util.Vector;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.provider.Contacts.People;
import android.provider.ContactsContract.CommonDataKinds.Phone;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.thomas.linstol.R;
import com.thomas.linstol.datamodel.TeamItem;

@SuppressWarnings("deprecation")
public class TeamGridAdapter extends BaseAdapter implements OnClickListener {
	List<TeamItem> teams = null;
	Vector<View> viewArray = null;

	private Context mContext;

	// Gets the context so it can be used later
	public TeamGridAdapter(Context c, List<TeamItem> teams) {
		mContext = c;
		this.teams = teams;
		viewArray = new Vector<View>();
		for (int i = 0; i < teams.size(); i++) {
			viewArray.addElement(null);
		}
	}

	// Total number of things contained within the adapter
	public int getCount() {
		return teams.size();
	}

	// Require for structure, not really used in my code.
	public Object getItem(int position) {
		return null;
	}

	// Require for structure, not really used in my code. Can
	// be used to get the id of an item in the adapter for
	// manual control.
	public long getItemId(int position) {
		return position;
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		View row = viewArray.elementAt(position);
		if (row == null) {
			LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
			row = inflater.inflate(R.layout.team_item, parent, false);

			viewArray.setElementAt(row, position);
			DisplayImageOptions options = new DisplayImageOptions.Builder()
					.cacheOnDisk(true).showImageOnFail(R.drawable.loading)
					.showImageForEmptyUri(R.drawable.loading)
					.showImageOnLoading(R.drawable.loading).build();

			TeamItem item = teams.get(position);
			ImageView imgView = (ImageView) row
					.findViewById(R.id.photoImageView);
			ImageLoader.getInstance().displayImage(item.getPhotoUrl(), imgView,
					options);
			((TextView) row.findViewById(R.id.nameLabel)).setText(item
					.getName());
			((TextView) row.findViewById(R.id.positionLabel)).setText(item
					.getPosition());
			((TextView) row.findViewById(R.id.phoneNumberLabel)).setText(item
					.getMobile());
			((TextView) row.findViewById(R.id.phoneNumberLabel))
					.setTag(position);
			((TextView) row.findViewById(R.id.phoneNumberLabel))
					.setOnClickListener(this);
			;

			String mailLink = "<a href=\"mailto:" + item.getEmail() + "\">"
					+ item.getEmail() + "</a>";
			((TextView) row.findViewById(R.id.emailLabel)).setText(Html
					.fromHtml(mailLink));
			((TextView) row.findViewById(R.id.emailLabel))
					.setMovementMethod(LinkMovementMethod.getInstance());

			row.findViewById(R.id.addContactButton).setOnClickListener(this);
			row.findViewById(R.id.addContactButton).setTag(position);
		}

		return row;
	}

	@Override
	public void onClick(View v) {
		TeamItem item = teams.get((Integer) v.getTag());
		if (v.getId() == R.id.phoneNumberLabel) {
			Intent intent = new Intent(Intent.ACTION_CALL);
			intent.setData(Uri.parse("tel:" + item.getMobile()));
			mContext.startActivity(intent);
		} else if (v.getId() == R.id.addContactButton) {
			this.addContact(item.getName(), item.getMobile());
		}
	}

	private void addContact(String name, String phone) {
		ContentValues values = new ContentValues();
		values.put(People.NUMBER, phone);
		values.put(People.TYPE, Phone.TYPE_CUSTOM);
		values.put(People.LABEL, name);
		values.put(People.NAME, name);
		Uri dataUri = mContext.getContentResolver().insert(People.CONTENT_URI,
				values);
		Uri updateUri = Uri.withAppendedPath(dataUri,
				People.Phones.CONTENT_DIRECTORY);
		values.clear();
		values.put(People.Phones.TYPE, People.TYPE_MOBILE);
		values.put(People.NUMBER, phone);
		updateUri = mContext.getContentResolver().insert(updateUri, values);

		new AlertDialog.Builder(mContext)
				.setTitle("Alert")
				.setMessage(name + " is added to your contacts")
				.setPositiveButton(android.R.string.yes,
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,
									int which) {
								// continue with delete
							}
						}).show();
	}
}