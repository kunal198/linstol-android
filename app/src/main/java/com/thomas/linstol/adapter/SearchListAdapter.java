package com.thomas.linstol.adapter;

import java.util.List;
import java.util.Vector;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.thomas.linstol.MainActivity;
import com.thomas.linstol.R;
import com.thomas.linstol.datamodel.OfficeItem;
import com.thomas.linstol.datamodel.ProductItem;
import com.thomas.linstol.datamodel.TeamItem;
import com.thomas.linstol.fragment.LocationDetailFragment;
import com.thomas.linstol.fragment.ProductDetailFragment;
import com.thomas.linstol.fragment.TeamListFragment;
import com.thomas.linstol.widget.SectionAdapter;

public class SearchListAdapter extends SectionAdapter implements
		OnClickListener {

	private List<List<?>> searchItems = null;
	private String[] headers = { "Products", "Notifications", "Team",
			"Locations" };
	private Context mContext = null;

	public SearchListAdapter(Context context, List<List<?>> searchItems) {
		this.mContext = context;
		this.searchItems = searchItems;
	}

	@Override
	public int numberOfSections() {
		return searchItems.size();
	}

	@Override
	public int numberOfRows(int section) {
		if (section >= 0 && section < searchItems.size()) {
			if (section == 0) {
				int t_count = searchItems.get(section).size();
				return (t_count / 2) + (t_count % 2 > 0 ? 1 : 0);
			} else {
				return searchItems.get(section).size();
			}
		} else {
			return 0;
		}
	}

	@Override
	public boolean hasSectionHeaderView(int section) {
		return section >= 0 && section < searchItems.size();
	}

	@Override
	public View getRowView(int section, int row, View convertView,
			ViewGroup parent) {
		if (section == 0) {
			convertView = ((Activity) mContext).getLayoutInflater().inflate(
					R.layout.search_product_item, null);
			LinearLayout mainLayout = (LinearLayout) convertView
					.findViewById(R.id.mainLayout);
			int count = mainLayout.getChildCount();
			DisplayImageOptions options = new DisplayImageOptions.Builder()
					.cacheOnDisk(true).showImageOnFail(R.drawable.loading)
					.showImageForEmptyUri(R.drawable.loading)
					.showImageOnLoading(R.drawable.loading).build();

			for (int i = 0; i < count; i++) {
				View itemView = mainLayout.getChildAt(i);
				try {
					ProductItem item = (ProductItem) searchItems.get(section)
							.get(count * row + i);
					itemView.setVisibility(View.VISIBLE);
					itemView.setTag(count * row + i);
					itemView.setOnClickListener(this);
					ImageLoader.getInstance()
							.displayImage(
									item.getPhotoUrl(),
									(ImageView) itemView
											.findViewById(R.id.photoImageView),
									options);
					((TextView) itemView.findViewById(R.id.titleLabel))
							.setText(item.getProductName());
				} catch (Exception e) {
					itemView.setVisibility(View.INVISIBLE);
				}
			}
		} else {
			convertView = (TextView) ((Activity) mContext).getLayoutInflater()
					.inflate(R.layout.inbox_row_item, null);

			if (section == 2) {
				((TextView) convertView).setText(((TeamItem) searchItems.get(
						section).get(row)).getName());
				Log.e("two","two");

			} else if (section == 3) {
				((TextView) convertView).setText(((OfficeItem) searchItems.get(
						section).get(row)).getLocationName());
				Log.e("three", "three");

			}

		}

		return convertView;
	}

	@Override
	public Object getRowItem(int section, int row) {
		return null;
	}

	@Override
	public int getSectionHeaderViewTypeCount() {
		return 2;
	}

	@Override
	public int getSectionHeaderItemViewType(int section) {
		if (section == 0)
			return 0;
		else
			return 1;
	}

	@Override
	public View getSectionHeaderView(int section, View convertView,
			ViewGroup parent) {
		if (convertView == null) {
			convertView = (TextView) ((Activity) mContext).getLayoutInflater()
					.inflate(R.layout.inbox_section_item, null);
		}
		if(searchItems.get(section).size()>=1) {
			TextView tvTitle=(TextView)convertView.findViewById(R.id.sectionTextView);
			tvTitle.setText(headers[section]);
			tvTitle.setVisibility(View.VISIBLE);
			Log.e("visibible", "visible");
		}
		return convertView;
	}

	@Override
	public void onRowItemClick(AdapterView<?> parent, View view, int section,
			int row, long id) {
		if (section == 2) {
			TeamItem teamInfo = (TeamItem) searchItems.get(section).get(row);
			Vector<TeamItem> team = new Vector<TeamItem>();
			team.addElement(teamInfo);
			TeamListFragment fragment = new TeamListFragment(team);
			((MainActivity) mContext).pushFragment(fragment);
		} else if (section == 3) {
			OfficeItem item = (OfficeItem) searchItems.get(section).get(row);
			LocationDetailFragment fragment = new LocationDetailFragment(item);
			((MainActivity) mContext).pushFragment(fragment);
		}
	}

	@Override
	public void onClick(View v) {
		ProductItem item = (ProductItem) searchItems.get(0).get(
				(int) v.getTag());
		ProductDetailFragment fragment = new ProductDetailFragment(item);
		((MainActivity) mContext).pushFragment(fragment);
	}

}
