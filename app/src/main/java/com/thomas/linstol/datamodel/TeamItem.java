package com.thomas.linstol.datamodel;

import org.json.JSONException;
import org.json.JSONObject;

public class TeamItem {
	private String teamId;
	private String name;
	private String position;
	private String mobile;
	private String photoUrl;
	private String email;

	public TeamItem(JSONObject jsonObj) {
		try {
			teamId = jsonObj.getString("ID");
		} catch (JSONException e) {
			teamId = "";
		}
		try {
			name = jsonObj.getString("firstName") + " "
					+ jsonObj.getString("lastName");
		} catch (JSONException e) {
			name = "";
		}
		try {
			position = jsonObj.getString("title");
		} catch (JSONException e) {
			position = "";
		}
		try {
			mobile = jsonObj.getString("mobile").equals("") ? jsonObj
					.getString("mobileUK") : jsonObj.getString("mobile");
		} catch (JSONException e) {
			mobile = "";
		}
		try {
			email = jsonObj.getString("email");
		} catch (JSONException e) {
			email = "";
		}
		try {
			photoUrl = jsonObj.getString("imgUrl");
		} catch (JSONException e) {
			photoUrl = "";
		}
	}
	
	public String getTeamId() {
		return teamId;
	}

	public void setTeamId(String teamId) {
		this.teamId = teamId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public String getMobile() {
		if (mobile == null) {
			return "";
		} else {
			String formatedNumber = "";
			for (int i=0; i<mobile.length(); i++) {
				char ch = mobile.charAt(i);
				if (ch == '+' || (ch >= '0' && ch <= '9')) {
					formatedNumber += ch;
				}
			}
			return formatedNumber;
		}
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getPhotoUrl() {
		return photoUrl;
	}

	public void setPhotoUrl(String photoUrl) {
		this.photoUrl = photoUrl;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
}
