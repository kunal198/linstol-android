package com.thomas.linstol.datamodel;

import org.json.JSONException;
import org.json.JSONObject;

public class CategoryItem {

	private String categoryId;
	private String categoryName;
	private String photoUrl;

	public CategoryItem(JSONObject jsonObj) {
		try {
			categoryId = jsonObj.getString("categoryId");
		} catch (JSONException e) {
			categoryId = "";
		}

		try {
			setCategoryName(jsonObj.getString("category"));
		} catch (JSONException e) {
			setCategoryName("");
		}

		try {
			photoUrl = jsonObj.getString("imgUrl");
		} catch (JSONException e) {
			photoUrl = "";
		}
	}

	public String getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(String categoryId) {
		this.categoryId = categoryId;
	}

	public String getPhotoUrl() {
		return photoUrl;
	}

	public void setPhotoUrl(String photoUrl) {
		this.photoUrl = photoUrl;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

}
