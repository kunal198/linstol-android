package com.thomas.linstol.datamodel;

import org.json.JSONException;
import org.json.JSONObject;

public class NotificationItem {
	private String notificationId;
	private String title;
	private String content;
	private String imageUrl;
	private String productId;
	private String dateStr;

	public NotificationItem(JSONObject jsonObj) {
		try {
			notificationId = jsonObj.getString("ID");
		} catch (JSONException e) {
			notificationId = "";
		}
		try {
			title = jsonObj.getString("title");
		} catch (JSONException e) {
			title = "";
		}
		try {
			content = jsonObj.getString("content");
		} catch (JSONException e) {
			content = "";
		}
		try {
			imageUrl = jsonObj.getString("imageUrl");
		} catch (JSONException e) {
			imageUrl = "";
		}
		try {
			productId = jsonObj.getString("productID");
		} catch (JSONException e) {
			productId = "";
		}
		try {
			dateStr = jsonObj.getString("dateStr");
		} catch (JSONException e) {
			dateStr = "";
		}
	}

	public String getNotificationId() {
		return notificationId;
	}

	public void setNotificationId(String notificationId) {
		this.notificationId = notificationId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public String getDateStr() {
		return dateStr;
	}

	public void setDateStr(String dateStr) {
		this.dateStr = dateStr;
	}

}
