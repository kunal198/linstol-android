package com.thomas.linstol.datamodel;

import org.json.JSONException;
import org.json.JSONObject;

public class FeatureItem {

	public String getImgUrl() {
		return imgUrl;
	}

	public void setImgUrl(String imgUrl) {
		this.imgUrl = imgUrl;
	}

	private String imgUrl;

	public FeatureItem(JSONObject jsonObj) {
		try {
			imgUrl = jsonObj.getString("imgUrl");
		} catch (JSONException e) {
			imgUrl = "";
		}


	}

}
