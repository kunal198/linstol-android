package com.thomas.linstol.datamodel;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class ProductItem {
	private String productId;
	private String productName;
	private String photoUrl;
	private String orgPhotoUrl;
	private String productDetails;



	private ArrayList<String> imgUrl=new ArrayList<>();

	public ProductItem(JSONObject jsonObj) {
		try {
			productId = jsonObj.getString("ID");
		} catch (JSONException e) {
			productId = "";
		}
		try {
			productName = jsonObj.getString("name");
		} catch (JSONException e) {
			productName = "";
		}
		try {
			photoUrl = jsonObj.getString("imgUrl");
		} catch (JSONException e) {
			photoUrl = "";
		}
		try {
			orgPhotoUrl = jsonObj.getString("orgImgUrl");
		} catch (JSONException e) {
			orgPhotoUrl = "";
		}
		try {
			productDetails = jsonObj.getString("description");
		} catch (JSONException e) {
			productDetails = "";
		}
		try {
			imgUrl.add(jsonObj.getString("imgUrl"));
			JSONObject objAdditionalImages=jsonObj.getJSONObject("AdditionalImages");
			JSONObject two=objAdditionalImages.getJSONObject("2");
			for(int i=0;i<two.length();i++)
			{
				imgUrl.add(two.getString("orgImgUrl"));
			}
		} catch (JSONException e) {
			e.printStackTrace();
			Log.e("issuess", "issues");
		}

	}
	public ArrayList<String> getImgUrl() {
		return imgUrl;
	}

	public void setImgUrl(ArrayList<String> imgUrl) {
		this.imgUrl = imgUrl;
	}
	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getPhotoUrl() {
		return photoUrl;
	}

	public void setPhotoUrl(String photoUrl) {
		this.photoUrl = photoUrl;
	}

	public String getOrgPhotoUrl() {
		return orgPhotoUrl;
	}

	public void setOrgPhotoUrl(String orgPhotoUrl) {
		this.orgPhotoUrl = orgPhotoUrl;
	}

	public String getProductDetails() {
		return productDetails;
	}

	public void setProductDetails(String productDetails) {
		this.productDetails = productDetails;
	}

}
