package com.thomas.linstol.datamodel;

import java.util.Calendar;
import java.util.TimeZone;

import org.json.JSONException;
import org.json.JSONObject;

import com.thomas.linstol.R;

public class OfficeItem {
	private String officeId;
	private int locationId;
	private String locationName;
	private String address;
	private String mobile;
	private String email;
	private String timezone;
	private double longtitude;
	private double lattitude;
	private int mapImageId;
	private int mapLargeImageId;

	private String[] regionArray = { "United States", "United Kingdom",
			"Hong Kong", "China" };

	private String[] timeArray = { "GMT-04:00", "GMT+01:00", "GMT+08:00",
			"GMT+08:00" };

	private int[] mapImageArray = { R.drawable.linstol_map_us,
			R.drawable.linstol_map_uk, R.drawable.linstol_map_hk,
			R.drawable.linstol_map_cn };

	private int[] mapLargeImageArray = { R.drawable.map_img_us,
			R.drawable.map_img_uk, R.drawable.map_img_hk, R.drawable.map_img_cn };

	public OfficeItem(JSONObject jsonObj) {
		try {
			officeId = jsonObj.getString("ID");
		} catch (JSONException e) {
			officeId = "";
		}
		try {
			locationId = jsonObj.getInt("location");
		} catch (JSONException e) {
			locationId = 1;
		}

		locationName = regionArray[locationId - 1];
		mapImageId = mapImageArray[locationId - 1];
		mapLargeImageId = mapLargeImageArray[locationId - 1];

		try {
			address = jsonObj.getString("address");
		} catch (JSONException e) {
			address = "";
		}
		try {
			mobile = jsonObj.getString("mobile");
		} catch (JSONException e) {
			mobile = "";
		}
		try {
			email = jsonObj.getString("email");
		} catch (JSONException e) {
			email = "";
		}
		try {
			timezone = jsonObj.getString("timezone");
		} catch (JSONException e) {
			timezone = "";
		}
		try {
			lattitude = Double.parseDouble(jsonObj.getString("lat"));
		} catch (JSONException e) {
			lattitude=0.0;
		}
		try {
			longtitude = Double.parseDouble(jsonObj.getString("lng"));
		} catch (JSONException e) {
			longtitude=0.0;
		}

	}

	public String getOfficeId() {
		return officeId;
	}

	public void setOfficeId(String officeId) {
		this.officeId = officeId;
	}

	public int getLocationId() {
		return locationId;
	}

	public void setLocationId(int locationId) {
		this.locationId = locationId;
	}

	public String getLocationName() {
		return locationName;
	}

	public void setLocationName(String locationName) {
		this.locationName = locationName;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public double getLongtitude() {
		return longtitude;
	}

	public void setLongtitude(double longtitude) {
		this.longtitude = longtitude;
	}

	public double getLattitude() {
		return lattitude;
	}

	public void setLattitude(double lattitude) {
		this.lattitude = lattitude;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getTimezone() {
		return timezone;
	}

	public void setTimezone(String timezone) {
		this.timezone = timezone;
	}

	public int getMapImageId() {
		return mapImageId;
	}

	public void setMapImageId(int mapImageId) {
		this.mapImageId = mapImageId;
	}

	public int getMapLargeImageId() {
		return mapLargeImageId;
	}

	public void setMapLargeImageId(int mapLargeImageId) {
		this.mapLargeImageId = mapLargeImageId;
	}

	public String getCurrentTime() {
		TimeZone tz = TimeZone.getTimeZone(timeArray[locationId - 1]);
		Calendar c = Calendar.getInstance(tz);
		String time = String.format("%02d", c.get(Calendar.HOUR)) + ":"
				+ String.format("%02d", c.get(Calendar.MINUTE)) + " "
				+ (c.get(Calendar.AM_PM) == Calendar.AM ? "AM" : "PM");

		return time;
	}

}
