package com.thomas.linstol.datamodel;

import android.content.Context;
import android.content.SharedPreferences;

public class NotificationModel {
	private static final String MY_PREFS_NAME = "Linstol";

	public static void checkRead(String notificationId, Context context) {
		SharedPreferences.Editor editor = context.getSharedPreferences(
				MY_PREFS_NAME, Context.MODE_PRIVATE).edit();
		editor.putBoolean(notificationId, true);
		editor.commit();
	}

	public static boolean isReadNotification(String notificationId,
			Context context) {
		return context
				.getSharedPreferences(MY_PREFS_NAME, Context.MODE_PRIVATE)
				.contains(notificationId);
	}
}
